# Git Workshop
Lab 6: Stashing Changes

---

## Exercises

 - Use the following command to clone a repo:
```
$ git clone https://gitlab.com/git-getstarted/lab6.git
```

 - Edit the existent file and add the changes to the index:
```
$ echo first update >> file.txt
$ git add file.txt
```

 - Create a new file (don't add it to the index)
```
$ echo new file content > new.txt
```

 - Check the repository status 
```
$ git status
```

 - Stash the changes 
```
$ git stash
```

 - Check the repository status
```
$ git status
```

 - Edit the file again and add the changes
```
$ echo second update >> file.txt
$ git add file.txt
```

 - Check the repository status
```
$ git status
```

 - This time stash the changes including the untracked
```
$ git stash --include-untracked
```

 - Check the repository status
```
$ git status
```

 - See the existent stashes
```
$ git stash list
```

 - Delete the first stash from the list
```
$ git stash drop stash@{0}
```

 - See the existent stashes
```
$ git stash list
```

 - Unstash the changes using
```
$ git stash apply stash@{0}
```

 - Check the repository status
```
$ git status
```

 - Check the file.txt content, which stash was retrieved?
```
$ cat file.txt
```

---

## Tips

 - You can see your existent stashes using
```
$ git stash list
```

 - You can delete all your stashes using
```
$ git stash clear
```
